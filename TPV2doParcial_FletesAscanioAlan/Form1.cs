﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TPV2doParcial
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TextWriter Escribir = new StreamWriter("Text.txt");
            Escribir.WriteLine(
                "LOCALIDADINVENTARIO =\n\n" +
                
                "install.option =\n\n" +
               
                 "LOCALIDADEBASE=\n\n" +
                
                "install.password =\n\n" +
               
                "install.redundancia = ");
            Escribir.Close();
            MessageBox.Show("El archivo se ha creado exitosamente");

            button1.Enabled = true;
            button3.Enabled = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TextReader Leer = new StreamReader("Text.txt");
            MessageBox.Show(Leer.ReadToEnd());
            Leer.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try { 
            openFileDialog1.Title = "Busca tu archivo -TB";
            openFileDialog1.ShowDialog();
            string Texto = openFileDialog1.FileName;

            if (File.Exists(openFileDialog1.FileName))
            {
                TextReader Leer = new StreamReader(Texto);
                richTextBox1.Text = Leer.ReadToEnd();
                Leer.Close();
            }
                    textBox1.Text = Texto;
                
                button4.Enabled = true;
                button2.Enabled = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Error al Abrir");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(saveFileDialog1.FileName))
                    {
                        string txt = saveFileDialog1.FileName;
                        StreamWriter textoguardar = File.CreateText(txt);
                        textoguardar.Write(richTextBox1.Text);
                        textoguardar.Flush();
                        textoguardar.Close();

                        textBox1.Text = txt;
                    }
                    else
                    {
                        string txt = saveFileDialog1.FileName;
                        StreamWriter textoguardar = File.CreateText(txt);
                        textoguardar.Write(richTextBox1.Text);
                        textoguardar.Flush();
                        textoguardar.Close();

                        textBox1.Text = txt;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error Al Guardar");
            }
        }
    }
}
//para usar el codigo se tiene que crear primero el archivo despues se activan los votones leer y abrir 
//Desplegar Datos solo sirve para leer los datos ya sea que este vacio o este con los datos 
//Despues se tiene que abrir el archivo txt creado con los datos soliciados
//apareceran los datos en el recuadro y ahi es donde se puede editar la info 
//luego le da guardar y lo puede sobreescribir o puede guardarlo como otro archivo pero le tiene que agrear el formato .txt